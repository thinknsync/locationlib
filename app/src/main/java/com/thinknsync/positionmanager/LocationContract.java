package com.thinknsync.positionmanager;

import com.thinknsync.observerhost.TypedObserver;

import java.util.List;

/**
 * Created by shuaib on 2/27/18.
 */

public interface LocationContract {
    interface View {
        void onFirstLocationReceived(TrackingData locationData);
    }

    interface Presenter {
        void initLocationServices(List<TypedObserver<TrackingData>> observers);
    }
}
