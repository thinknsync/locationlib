package com.thinknsync.positionmanager;

public interface LocationListenerAction {
    void onLocationReceived(TrackingData locationData);
}
