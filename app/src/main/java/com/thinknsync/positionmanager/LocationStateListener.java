package com.thinknsync.positionmanager;

/**
 * Created by letsgoandroid on 8/13/17.
 */

public interface LocationStateListener {
    boolean isLocationServicesOn();
    void showLocationServicesDialog();
}
