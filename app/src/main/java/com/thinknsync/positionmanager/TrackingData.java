package com.thinknsync.positionmanager;

import com.thinknsync.convertible.BaseConvertible;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shuaib on 10/19/16.
 */

public final class TrackingData extends BaseConvertible<TrackingData> {

    public static final String key_lat = "lat";
    public static final String key_lon = "lon";
    public static final String key_accuracy = "accuracy";
    public static final String key_timestamp = "timestamp";

    private double lat;
    private double lon;
    private float accuracy;
    private long timeStamp;

    public TrackingData() {
    }

    public TrackingData(double lat, double lon) {
        setLat(lat);
        setLon(lon);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        return new JSONObject().put(key_lat, getLat())
                .put(key_lon, getLon())
                .put(key_accuracy, getAccuracy())
                .put(key_timestamp, getTimeStamp());
    }

    @Override
    public TrackingData fromJson(JSONObject jsonObject) throws JSONException {
        setLat(jsonObject.getDouble(key_lat));
        setLon(jsonObject.getDouble(key_lon));
        setAccuracy(jsonObject.optLong(key_accuracy));
        setTimeStamp(jsonObject.optLong(key_timestamp));

        return this;
    }
//
//    public double getDistanceBetween(TrackingData locationData){
//        double d2r = Math.PI / 180;
//
//        double dlong = (getLon() - locationData.getLon()) * d2r;
//        double dlat = (getLat() - locationData.getLat()) * d2r;
//        double a = Math.pow(Math.sin(dlat / 2.0), 2) + Math.cos(locationData.getLat() * d2r)
//                * Math.cos(getLat() * d2r) * Math.pow(Math.sin(dlong / 2.0), 2);
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//        double d = 6367 * c;
//        return d * 1000;
//    }

}
